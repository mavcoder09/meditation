package trainthemonkey.com.meditation;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;

import com.google.firebase.analytics.FirebaseAnalytics;

public class MainActivity extends Activity {

    private FirebaseAnalytics mFirebaseAnalytics;
    ImageButton btnplay;
    ImageButton btnpause;
    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "new user");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
        btnplay=(ImageButton)findViewById(R.id.btnPlay);
        //btnpause=(ImageButton)findViewById(R.id.btnPause);
        mp = MediaPlayer.create(MainActivity.this, R.raw.meditation);

        mp.start();
        btnplay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check for already playing
                if(mp.isPlaying()){
                    if(mp!=null){
                       //
                        mp.pause();
  //                      btnplay.setVisibility(View.GONE);
//                        btnpause.setVisibility(View.VISIBLE);
                        btnplay.setImageResource(R.drawable.btn_pause);
                        //System.out.println("Pause clicked");
                        // Changing button image to play button

                    }
                }else{
                    // Resume song
                    if(mp!=null){

                        mp.start();
                      //  System.out.println("play clicked");
                        // Changing button image to pause button
                      btnplay.setImageResource(R.drawable.btn_play);
                        //btnpause.setVisibility(View.GONE);
                        //btnplay.setVisibility(View.VISIBLE);

                    }
                }

            }
        });



//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
//        if(mp.isPlaying()) {
//            if (mp != null) {
//                //
//                mp.pause();
//                //                      btnplay.setVisibility(View.GONE);
////                        btnpause.setVisibility(View.VISIBLE);
//                btnplay.setImageResource(R.drawable.btn_pause);
//                //System.out.println("Pause clicked");
//                // Changing button image to play button
//            }
//        }
        super.onPause();
    }

    @Override
    protected void onResume() {

//        if(mp!=null){
//
//            mp.start();
//            //  System.out.println("play clicked");
//            // Changing button image to pause button
//            btnplay.setImageResource(R.drawable.btn_play);
//            //btnpause.setVisibility(View.GONE);
//            //btnplay.setVisibility(View.VISIBLE);
//
//        }
        super.onResume();
    }

}
